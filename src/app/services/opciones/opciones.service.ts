import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OpcionesService {
  public URL_SERVER = environment.baseUrl
  public URL_ROOT_WS = this.URL_SERVER + '/api/principal';
    constructor(private http: HttpClient) {

  }


  getbuscarOpcionesUsuario(usuario:number): Observable<any> {
    console.log(this.URL_ROOT_WS + '/buscarOpcionesUsuario/'+usuario);
    return this.http.get(this.URL_ROOT_WS + '/buscarOpcionesUsuario/'+usuario);
  }
}

import { Component, OnDestroy, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from '../../_nav';
import { AuthService } from '../../services/auth/auth.service';
import { AccountService } from '../../services/auth/account.service';
import { OpcionesService } from '../../services/opciones/opciones.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy {
  
  public sidebarMinimized = false;
  public navItems = navItems;

  
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  private changes: MutationObserver;
  public element: HTMLElement;
  dataOpciones: any = []
  dataOpcionesHijos: any = []
  dataOpcionesPadre: any = []
  loginUser: any = {}
  opciones:any=[]
  // dataOpcionTotal:any=[]
  constructor(@Inject(DOCUMENT) _document?: any, opcionesServicio?: OpcionesService,private  authService?: AuthService, private accountService?:AccountService) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;

    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
    this.loginUser = this.accountService.getUsuario();
    console.log( this.loginUser)
    // this.authService.getUserInfo().subscribe(data => {
    //   this.loginUser = data;
    //  console.log(this.loginUser.id)
      //console.log("entro aqui")
      opcionesServicio.getbuscarOpcionesUsuario(this.loginUser.id).subscribe(dataPadre => {
        console.log("dataPadre")
        console.log(dataPadre)
          for (let i = 0; i < dataPadre.length; i++) {
            if(dataPadre[i].padreId==null && dataPadre[i].principal==1){
            this.opciones.push({
              'id':dataPadre[i].id,
              'name':dataPadre[i].nombre,
              'url':dataPadre[i].url,
              // 'url':"#",
              'icon':dataPadre[i].icono
            });
            }else{
            this.traverseRespaldo(this.opciones,dataPadre[i]);
            }
          }
        this.navItems = this.opciones
        console.log("this.navItems")
      
        })
   // })
  }
    
  traverseRespaldo(obj: any, opc:any): any {
    for ( var i in obj) {
      if(obj[i].id==opc.padreId &&opc.principal==1){
        if(this.opciones[i].children==null){
          this.opciones[i].children=[];
        }
        this.opciones[i].children.push({
          'id':opc.id,
          'name':opc.nombre,
          'url':opc.url,
          'icon':opc.icono
        })
       return 
      }
      if(typeof ( obj[i].children) == "object" ){

        this.traverseRespaldo(obj[i].children , opc);
      } 
    }
}


  ngOnDestroy(): void {
    this.changes.disconnect();
  }

}

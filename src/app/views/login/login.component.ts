import { Component ,Input, ViewChild, OnInit } from '@angular/core';
import { Usuario } from '../../model/usuario'
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  
  @Input() title = 'Sistema ';
  usuario: Usuario = new Usuario();
  @Input() mensajeNotificacion='Credenciales no válidas';
 // @ViewChild('msgNotification') msgNotification: jqxNotificationComponent;
 
  constructor(//public messagerService: MessagerService,
              private router: Router,
              private autenticacionService: AuthService) { }

  ngOnInit() {
    this.autenticacionService.logout();
  }

  gotoHome() {
    //this.router.navigate(['/control']);
   
   this.router.navigate(['/dashboard']);
   
  }
///codigo ayuda peter
  getUser(){
    console.log("ente");
    this.autenticacionService.getUserInfo()
    .subscribe(data => {
      // Si se obtuvo el token lo almacena en memoria en notación JSON.
      console.log(" user " + JSON.stringify(data));
     
      localStorage.setItem('usuarioActual', JSON.stringify(data));
      //this.tipoUsuario();
    }, error => {
      alert('paso algo corre');
      /*  this.messagerService.alert({
        title: 'Credenciales no válidas',
        msg: 'Ingrese correctamente sus credenciales de acceso!'
       }); */
      console.log(error);
    });
  }


  login() {
    //localStorage.setItem('usuarioActivo', JSON.stringify(this.usuario.usuario));

    this.autenticacionService.login(this.usuario.usuario, this.usuario.claveAuxiliar)
    .subscribe(data => {
      // Si se obtuvo el token lo almacena en memoria en notación JSON.
      localStorage.setItem('usuarioActual', JSON.stringify(data));
      //localStorage.getItem('usuarioActual');
      this.getUser();
      this.gotoHome();
    }, error => {
      alert('Credenciales no válidas');
      /*  this.messagerService.alert({
        title: 'Credenciales no válidas',
        msg: 'Ingrese correctamente sus credenciales de acceso!'
       }); */
      console.log(error);
    });

  }
 }

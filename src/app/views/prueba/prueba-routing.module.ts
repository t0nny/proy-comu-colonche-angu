import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ControlAutenticacion } from '../../security/control-autenticacion';
import { PersonaComponent } from './persona/persona.component';

const routes: Routes = [{
  path: '',
  
 
  data: {
    title: 'persona',
  },
  children: [
    
    {
      path:'persona',
      component: PersonaComponent,
      //canActivate: [ControlAutenticacion],
      data:{
        title: 'persona'
      }
    },

  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PruebaRoutingModule { }
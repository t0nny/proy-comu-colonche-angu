import { NgModule } from '@angular/core';

import { FormsModule }  from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PersonaComponent } from './persona/persona.component';
import { PruebaRoutingModule } from './prueba-routing.module';

@NgModule({
  declarations: [PersonaComponent],
  imports: [
    CommonModule,PruebaRoutingModule
  ] 
  , providers:[PruebaRoutingModule ]
})
export class PruebaModule { }
